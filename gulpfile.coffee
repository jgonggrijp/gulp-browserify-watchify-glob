gulp = require 'gulp'
{ src, dest, series, parallel } = gulp
coffee = require 'gulp-coffee'
babel = require 'gulp-babel'
jasmine = require 'gulp-jasmine'

pumpify = require 'pumpify'
_ = require 'underscore'
yargs = require 'yargs'

srcFile = 'index.coffee'
testFile = 'test.coffee'
sources = [srcFile, testFile]

transpileCoffee = -> pumpify.obj coffee(bare: on), babel
    presets: [['@babel/env', targets: node: '6']]

compileFs = (paths) -> ->
    src paths
    .pipe transpileCoffee()
    .pipe dest '.'

build = compileFs srcFile
build.displayName = 'build'
compileTests = compileFs testFile
compileTests.displayName = 'compileTests'

jasmineOptions = {}
mainTasks = yargs.argv._
if mainTasks.length is 0 or mainTasks.includes 'watch'
    jasmineOptions.errorOnFail = off

runTests = ->
    src 'test.js'
    .pipe jasmine jasmineOptions

test = series parallel(build, compileTests), runTests

watch = (done) ->
    watcher = gulp.watch sources, {ignoreInitial: off}, test
    terminate = ->
        watcher.close()
        done()
    process.once 'SIGINT', terminate
    process.once 'SIGTERM', terminate

_.extend exports, {build, test, watch, default: watch}
