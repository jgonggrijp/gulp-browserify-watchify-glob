# Contributing to gulp-browserify-watchify-glob

The project is hosted at https://gitlab.com/jgonggrijp/gulp-browserify-watchify-glob.

Thank you for considering to contribute. You are welcome to create issue tickets and to submit merge requests.

When contributing code, please keep the following house rules in mind.

- Do your work on a separate branch, preferably `feature/<descriptive-name>`.
- Submit your merge request to `master` (default).
- New code should be fully tested.
- Manage packages with Yarn, not NPM.
- 4 space indents.

When developing, you can run `yarn gulp` to repeat the test suite on every change. Before pushing, run `yarn test` to have a single test run with a coverage report. To inspect what was not covered, run `yarn nyc report -r html` and then open `coverage/index.html` in your browser.

Did I leave any of your contribution questions unanswered? Please let me know what you think!
