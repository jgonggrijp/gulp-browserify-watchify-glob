# Changelog of gulp-browserify-watchify-glob

Newest version on top.


## 1.0.0 — 2019-10-24 (release)

- Made an explicit choice to support Node >= 6.
- Bugfix: isolated the cache of each `globbedBrowserify` instance.
- Bugfix: wait for the `ready` event before `.close()`ing a watcher in order to prevent deadlock.
- Switched the implementation from TypeScript to CoffeeScript.
- Made some implementation changes to be able to test more thoroughly.
- Added unittests with 100% coverage.
- Added GitLab pipeline for continuous monitoring of code quality.
- Completed the README.
- Added the CONTRIBUTING.md and this changelog.


## 1.0.0-3 — 2019-10-03 (hotfix)

- Bugfix: wrapped the `.close()` method from watchify so that additional file watching logic introduced by gulp-browserify-watchify-glob is also stopped.


## 1.0.0-2 — 2019-10-02 (hotfix)

- Bugfix: debounced update events because two chokidar instances might potentially trigger for the same file modification.


## 1.0.0-1 — 2019-09-29 (prerelease)

- Added a minimal README.


## 1.0.0-0 — 2019-09-27 (prerelease)

Initial version, extracted from a gulpfile that seemed to work. This version already had all the functionality, just at a lower quality.
